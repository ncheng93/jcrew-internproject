var rawArrow = [[0,-7], [1.5, -5], [-1.5, -5]];
var hitbox = [[0,-9], [2, -9], [0, 0], [-2, -9], [0,-9]];

var Arrow = function () {
    this.locOnFilm = 0; //where on film (x pixel coordinate) this arrow should point
    this.destinationPano = null; //which panorama this arrow should lead to;
    this.angle = 0;
    this.vshift = 0; //extra tilt when shifting image up and down
    this.msg = "test message";
    this.rollOver = false;
}

Arrow.prototype = {

    getPerspective: function () {
        return (this.vshift*-.3+.3);
    },

    //transform coordinates to point at ang
    transform: function (oldCoords, ang){
        var SCALE = 15;
        var xShift = WIDTH/2;                 //center of spinner on canvas
        var yShift = HEIGHT*DONGLE_LOCATION;  //center of spinner on cavas
        var SPIN_ORIGIN = 0;                  //relative to arrow, where should spinning pivot be?

        //ang += Math.PI/2;
        var self = this;
        return oldCoords.map(function(co){
            var newX, newY;
            var perspective = self.getPerspective();
            
            var c = Math.cos(ang);
            var s = Math.sin(ang);

            var oX = co[0];
            var oY = co[1] - SPIN_ORIGIN;

            newX = c*oX + s*oY;
            newY = -s*oX + c*oY;
         
            newY += SPIN_ORIGIN;

            newX = (newX*SCALE)+xShift;
            newY = newY*SCALE*perspective+yShift;

            return [newX, newY];
        });
    },

    //get onscreen cordinates of arrow verticies
    getCoords: function () {
        return this.transform(rawArrow,this.angle);
    },

    //get onscreen cordinates of arrow hitbox vertices
    getHitboxCoords: function () {
        return this.transform(hitbox,this.angle);
    },

    //checks if (x,y) is inside the polygon of this
    containsMouse: function(){
        var coords = this.getHitboxCoords();
        var xs = coords.map(function(c){return c[0]});
        var ys = coords.map(function(c){return c[1]});
        return Utils.pnpoly(7,xs ,ys ,Mouse.x,Mouse.y);
    },

    printInfo: function(allPanos, currentPano) {
        var i, from, to;
        for(i=0;i<allPanos.length;i++){
            if(allPanos[i] === currentPano) from = i;
            if(allPanos[i] === this.destinationPano) to = i;
        }
        console.log("arrow: "+from+", "+to+", "+this.locOnFilm);
    },

    draw: function (ctx) {
        var coords = this.getCoords();

        ctx.fillStyle = (this.rollOver ? SELECT_FILL_COLOR : FILL_COLOR);
        ctx.strokeStyle = (this.rollOver ? SELECT_OUTLINE_COLOR : OUTLINE_COLOR);
        ctx.lineWidth = DONGLE_LINE;
        ctx.lineJoin = 'miter';
        ctx.beginPath();
        ctx.moveTo(coords[0][0],coords[0][1]);
        for (var i = 0; i < coords.length; i++) {
            ctx.lineTo(coords[i][0],coords[i][1]);
        }
        ctx.lineTo(coords[0][0],coords[0][1]);
        ctx.closePath();
        ctx.fill();
        ctx.stroke();

        if(this.rollOver && this.msg.length > 0){
            ctx.font = ARROW_FONT;
            ctx.textAlign = 'center';

            var horizPadding = 3;
            var vertPadding = 8;
            var up = -15-vertPadding;
            var down = 15 + vertPadding*2;
            var w = ctx.measureText(this.msg).width+10;
            ctx.fillStyle = FILL_COLOR;
            ctx.fillRect(WIDTH/2-w/2-horizPadding,HEIGHT*.8+up,w+horizPadding*2,down);

            // ctx.fillStyle = 'black';
            // ctx.fillText(this.msg, WIDTH/2+2, HEIGHT * .8+2);

            ctx.fillStyle = 'white';
            ctx.fillText(this.msg, WIDTH/2, HEIGHT * .8);
        }
    }
}