var ShopButton = function (x,y,url) {
    this.x = x;
    this.y = y;
    this.url = url;
    this.rollover = false;
}

ShopButton.prototype.update = function(offX,offY) {
    this.rollover = Utils.pointInRect(this.x+offX, //top left x of rect
                                     this.y+offY, //top left y of rect
                                     100,        //width of rect
                                     37,        //height of rect
                                     Mouse.x,   //test point x
                                     Mouse.y);  //test point y
}

ShopButton.prototype.gotoStore = function() {
    window.open(this.url);
}

ShopButton.prototype.draw = function (offX,offY) {
    var img = (this.rollover ? 'images/shop_orange.png' : 'images/shop_black.png');
    context.drawImage(Content.getImage(img),this.x+offX,this.y+offY);
}