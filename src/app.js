var app;

init();

function init () {
    Content.createLoader();

    //preload assets

    Content.preloadImage('images/small_panos/Small_Panorama01.jpg');
    Content.preloadImage('images/small_panos/Small_Panorama02.jpg');
    Content.preloadImage('images/small_panos/Small_Panorama03.jpg');
    Content.preloadImage('images/small_panos/Small_Panorama04.jpg');
    Content.preloadImage('images/small_panos/Small_Panorama05.jpg');
    Content.preloadImage('images/small_panos/Small_Panorama06.jpg');
    Content.preloadImage('images/small_panos/Small_Panorama07.jpg');
    Content.preloadImage('images/small_panos/Small_Panorama08.jpg');
    Content.preloadImage('images/small_panos/Small_Panorama09.jpg');
    Content.preloadImage('images/small_panos/Small_Panorama10.jpg');
    Content.preloadImage('images/small_panos/Small_Panorama11.jpg');
    Content.preloadImage('images/small_panos/Small_Panorama12.jpg');
    Content.preloadImage('images/small_panos/Small_Panorama13.jpg');
    Content.preloadImage('images/small_panos/Small_Panorama14.jpg');
    Content.preloadImage('images/small_panos/Small_Panorama15.jpg');
    Content.preloadImage('images/small_panos/Small_Panorama16.jpg');
    Content.preloadImage('images/small_panos/Small_Panorama17.jpg');
    Content.preloadImage('images/small_panos/Small_Panorama18.jpg');
    Content.preloadImage('images/small_panos/Small_Panorama19.jpg');
    Content.preloadImage('images/small_panos/Small_Panorama20.jpg');
    Content.preloadImage('images/small_panos/Small_Panorama21.jpg');
    Content.preloadImage('images/small_panos/Small_Panorama22.jpg');
    Content.preloadImage('images/small_panos/Small_Panorama23.jpg');
    Content.preloadImage('images/small_panos/Small_Panorama24.jpg');

    Content.preloadImage('images/floor_black_1.png');
    Content.preloadImage('images/floor_black_2.png');
    Content.preloadImage('images/floor_black_3.png');
    Content.preloadImage('images/floor_orange_1.png');
    Content.preloadImage('images/floor_orange_2.png');
    Content.preloadImage('images/floor_orange_3.png');

    Content.preloadImage('images/hover/hover1.png');
    Content.preloadImage('images/hover/hover2.png');
    Content.preloadImage('images/hover/hover3.png');
    Content.preloadImage('images/hover/hover4.png');
    Content.preloadImage('images/hover/hover5.png');
    Content.preloadImage('images/hover/hover6.png');
    Content.preloadImage('images/hover/f1hover1.png');
    Content.preloadImage('images/hover/f1hover2.png');
    Content.preloadImage('images/hover/f1hover3.png');
    Content.preloadImage('images/hover/f1hover4.png');
    Content.preloadImage('images/hover/f1hover5.png');
    Content.preloadImage('images/hover/f2hover1.png');
    Content.preloadImage('images/hover/f2hover2.png');
    Content.preloadImage('images/hover/f2hover3.png');
    Content.preloadImage('images/hover/f2hover4.png');
    Content.preloadImage('images/hover/f2hover5.png');
    Content.preloadImage('images/hover/f2hover6.png');
    Content.preloadImage('images/hover/f2hover7.png');
    Content.preloadImage('images/hover/f2hover8.png');
    Content.preloadImage('images/hover/f2hover9.png');

    Content.preloadImage('images/shop_black.png');
    Content.preloadImage('images/shop_orange.png');

    Content.preloadImage('images/overlay.png');


    Content.loadThenStart(startApp);
}

function startApp () {
    app = new AppController();
    animate(); //begin self-calling animate function
}

overlay = true;

// JS can only open links with onclick events otherwise
// it will be blocked by browser
// so this is a super hacky way of doing that.
canvas.onclick = function () {
    overlay = false;
    try {
        var button = null;
        var pano = app.currentPano;

        for (var i = 0; i < pano.popups.length; i++) {
            if(pano.popups[i].expanded && pano.popups[i].shopBtn.rollover) {
                button = pano.popups[i].shopBtn;
            }
        };

        if(button != null) button.gotoStore();

    } catch (e) {}
}

function animate() {
    app.update();
    app.draw();

    if(overlay) {
        context.drawImage(Content.getImage('images/overlay.png'), 0, 0);
    }
  
    // request new frame
    requestAnimFrame(function() {
        animate();
    });
}