var PanoContent = {
    setup: function (controller){
        controller.floorButtons = [];
        
        controller.panos = [null, //first value null to allow indexing starting at 1 to match file names
        //                          IMAGE PATH                       WIDTH HEIGHT NAME
            new Panorama ('images/small_panos/Small_Panorama01.jpg', 3843, 500, "Shoes"),
            new Panorama ('images/small_panos/Small_Panorama02.jpg', 3977, 500),
            new Panorama ('images/small_panos/Small_Panorama03.jpg', 3964, 500),
            new Panorama ('images/small_panos/Small_Panorama04.jpg', 3998, 500),
            new Panorama ('images/small_panos/Small_Panorama05.jpg', 4090, 500),
            new Panorama ('images/small_panos/Small_Panorama06.jpg', 4052, 500),
            new Panorama ('images/small_panos/Small_Panorama07.jpg', 4043, 500, "Pants and Denim"),
            new Panorama ('images/small_panos/Small_Panorama08.jpg', 3985, 500),
            new Panorama ('images/small_panos/Small_Panorama09.jpg', 4014, 500, "Shirt Bar"),
            new Panorama ('images/small_panos/Small_Panorama10.jpg', 4152, 500),
            new Panorama ('images/small_panos/Small_Panorama11.jpg', 3955, 500),
            new Panorama ('images/small_panos/Small_Panorama12.jpg', 3886, 500, "Crewcuts"),
            new Panorama ('images/small_panos/Small_Panorama13.jpg', 3938, 500),
            new Panorama ('images/small_panos/Small_Panorama14.jpg', 3943, 500, "Wear to Work"),
            new Panorama ('images/small_panos/Small_Panorama15.jpg', 4064, 500, "Shoes and Accessories"),
            new Panorama ('images/small_panos/Small_Panorama16.jpg', 4113, 500),
            new Panorama ('images/small_panos/Small_Panorama17.jpg', 4075, 500),
            new Panorama ('images/small_panos/Small_Panorama18.jpg', 4010, 500),
            new Panorama ('images/small_panos/Small_Panorama19.jpg', 4107, 500),
            new Panorama ('images/small_panos/Small_Panorama20.jpg', 3964, 500),
            new Panorama ('images/small_panos/Small_Panorama21.jpg', 4022, 500, "Cashmere Bar"),
            new Panorama ('images/small_panos/Small_Panorama22.jpg', 3957, 500),
            new Panorama ('images/small_panos/Small_Panorama23.jpg', 3976, 500),
            new Panorama ('images/small_panos/Small_Panorama24.jpg', 3918, 500, "Entrance")];

        controller.currentPano = controller.panos[24];

        // from = panos[from] contains the arrow
        // to = clicking the arrow leads to panos[to]
        // x = x-pixel-coordinate of were the arrow should be located
        //                   FROM, TO, X

        //FIRST FLOOR
        controller.createPanoBridge(17,19,763);
        controller.createPanoBridge(17,18,3715);
        controller.createPanoBridge(18,20,2654);
        controller.createPanoBridge(18,17,3827);
        controller.createPanoBridge(19,22,384);
        controller.createPanoBridge(19,17,2436);
        controller.createPanoBridge(19,20,3483);
        controller.createPanoBridge(20,21,0);
        controller.createPanoBridge(20,19,1055);
        controller.createPanoBridge(20,18,1961);
        controller.createPanoBridge(21,20,897);
        controller.createPanoBridge(21,23,2969);
        controller.createPanoBridge(22,23,741);
        controller.createPanoBridge(22,24,1122);
        controller.createPanoBridge(22,19,3189);
        controller.createPanoBridge(23,24,1310);
        controller.createPanoBridge(23,22,2453);
        controller.createPanoBridge(23,21,3379);
        controller.createPanoBridge(24,23,765);
        controller.createPanoBridge(24,22,3690);

        //1st <-> 2nd
        controller.createPanoBridge(17,15,2750, "Floor 2");
        controller.createPanoBridge(15,17,3300, "Floor 1");

        //SECOND FLOOR
        controller.createPanoBridge(12,14,120);
        controller.createPanoBridge(12,13,3526);
        controller.createPanoBridge(13,12,868);
        controller.createPanoBridge(13,15,2885);
        controller.createPanoBridge(13,14,3790);
        controller.createPanoBridge(14,15,441);
        controller.createPanoBridge(14,12,3025);
        controller.createPanoBridge(14,13,3483);
        controller.createPanoBridge(15,16,56);
        controller.createPanoBridge(15,14,714);
        controller.createPanoBridge(15,13,1032);
        controller.createPanoBridge(16,15,6);

        //2nd <-> 3rd
        controller.createPanoBridge(12,10,600, "Floor 3");
        controller.createPanoBridge(10,12,3900, "Floor 2");


        //THIRD FLOOR
        controller.createPanoBridge(1,4,2430);
        controller.createPanoBridge(1,7,1531);
        controller.createPanoBridge(1,8,3350);
        controller.createPanoBridge(2,8,1916);
        controller.createPanoBridge(2,6,3074);
        controller.createPanoBridge(2,4,3675);
        controller.createPanoBridge(3,4,2975);
        controller.createPanoBridge(3,5,3750);
        controller.createPanoBridge(4,6,160);
        controller.createPanoBridge(4,5,1272, "Ludlow Shop");
        controller.createPanoBridge(4,3,1848, "Ludlow Shop");
        controller.createPanoBridge(4,2,2860);
        controller.createPanoBridge(4,1,3492);
        controller.createPanoBridge(5,4,302);
        controller.createPanoBridge(5,3,3590);
        controller.createPanoBridge(6,7,586);
        controller.createPanoBridge(6,4,2857);
        controller.createPanoBridge(6,2,3500);
        controller.createPanoBridge(7,11,530);
        controller.createPanoBridge(7,6,2140);
        controller.createPanoBridge(7,4,2650);
        controller.createPanoBridge(7,1,3515); //OVERLAP
        controller.createPanoBridge(8,1,3948); //OVERLAP
        controller.createPanoBridge(8,2,1092);
        controller.createPanoBridge(8,9,2880);
        controller.createPanoBridge(9,8,804);
        controller.createPanoBridge(9,10,2714);
        controller.createPanoBridge(9,11,3033);
        controller.createPanoBridge(10,11,2728);
        controller.createPanoBridge(10,9,3419);
        controller.createPanoBridge(11,10,204);
        controller.createPanoBridge(11,7,2788);
        controller.createPanoBridge(11,9,3168);



        //floors will be numbered in the order they are added
        //each button goes to the pano at the index specified
        controller.addFloorButton(24);
        controller.addFloorButton(15);
        controller.addFloorButton(10);

        //index of pano to add this to, src, x-position, y-position, width, height
        controller.addPopUp(9 , 'images/hover/hover1.png', 2000, 215, 305, 220, 'https://www.jcrew.com/mens_category/shirts/slimshirtshop/PRD~43683/43683.jsp');
        controller.addPopUp(10, 'images/hover/hover2.png', 2605, 205, 305, 220, 'https://www.jcrew.com/mens_category/sweaters/cotton/PRDOVR~A4988/A4988.jsp');
        controller.addPopUp(5 , 'images/hover/hover3.png', 2794, 229, 305, 220, 'https://www.jcrew.com/mens_feature/TheLudlowShop/Lightweight/PRDOVR~A0498/99103266637/A0498.jsp');
        controller.addPopUp(5 , 'images/hover/hover4.png', 2794, 350, 305, 220, 'https://www.jcrew.com/mens_category/suiting/PRD~A0503/A0503.jsp?nav_type=OBIN');
        controller.addPopUp(1 , 'images/hover/hover5.png',  818, 318, 305, 220, 'https://www.jcrew.com/mens_category/shoes/Kentonshoes/PRD~43275/43275.jsp');
        controller.addPopUp(7 , 'images/hover/hover6.png', 2008, 231, 305, 220, 'https://www.jcrew.com/mens_category/sportcoatsandvests/LudlowUnconstructedSportcoats/PRDOVR~A6264/A6264.jsp');

        controller.addPopUp(21, 'images/hover/f1hover1.png',  540, 261, 305, 220, 'https://www.jcrew.com/womens_category/jewelry/necklaces/PRDOVR~B1692/B1692.jsp');
        controller.addPopUp(24, 'images/hover/f1hover2.png', 3190, 318, 305, 220, 'https://www.jcrew.com/womens_category/outerwearandblazers/suitingjackets/PRDOVR~A0361/A0361.jsp');
        controller.addPopUp(21, 'images/hover/f1hover3.png', 2082, 380, 305, 220, 'https://www.jcrew.com/womens_category/knitstees/shortsleevetees/PRDOVR~A6963/A6963.jsp');
        controller.addPopUp(17, 'images/hover/f1hover4.png', 2462, 240, 305, 220, 'https://www.jcrew.com/womens_category/dresses/cocktail/PRDOVR~A6580/A6580.jsp');
        controller.addPopUp(24, 'images/hover/f1hover5.png',  547, 319, 305, 220, 'https://www.jcrew.com/womens_category/shirtsandtops/sleeveless/PRDOVR~A7259/A7259.jsp');

        controller.addPopUp(15, 'images/hover/f2hover1.png', 1714, 223, 305, 220, 'https://www.jcrew.com/womens_category/dresses/Day/PRDOVR~A7793/A7793.jsp');
        controller.addPopUp(14, 'images/hover/f2hover2.png', 1894, 321, 305, 220, 'https://www.jcrew.com/womens_category/dresses/cocktail/PRDOVR~A1743/A1743.jsp');
        controller.addPopUp(14, 'images/hover/f2hover3.png', 2184, 275, 305, 220, 'https://www.jcrew.com/womens_category/dresses/weartowork/PRDOVR~A4863/A4863.jsp');
        controller.addPopUp(14, 'images/hover/f2hover4.png', 2357, 328, 305, 220, 'https://www.jcrew.com/womens_category/handbags/Hobos/PRDOVR~B0218/B0218.jsp?color_name=old-lace');
        controller.addPopUp(13, 'images/hover/f2hover5.png', 1877, 361, 305, 220, 'https://www.jcrew.com/boys_category/swimrashguards/rashguards/PRDOVR~A7923/A7923.jsp?color_name=cool-slate');
        controller.addPopUp(13, 'images/hover/f2hover6.png', 1234, 355, 305, 220, 'https://www.jcrew.com/boys_category/swimrashguards/boardshorts/PRDOVR~A5748/A5748.jsp');
        controller.addPopUp(12, 'images/hover/f2hover7.png', 2664, 206, 305, 220, 'https://www.jcrew.com/boys_category/shirts/washedfavoriteshirts/PRDOVR~69122/69122.jsp?color_name=capri-orange');
        controller.addPopUp(12, 'images/hover/f2hover8.png', 1818, 231, 305, 220, 'https://www.jcrew.com/girls_category/dresses/everydaydresses/PRDOVR~A6638/A6638.jsp?color_name=dark-cove');
        controller.addPopUp(12, 'images/hover/f2hover9.png',  618, 340, 305, 220, 'https://www.jcrew.com/baby/onepieces/PRDOVR~A3455/A3455.jsp');

        //starting positions if you navigate straight to floor
        controller.panos[15].x = -584;
        controller.panos[10].x = -2855;
    }
}