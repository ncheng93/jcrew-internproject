var WIDTH = 800;
var HEIGHT = 500;
var DONGLE_LOCATION = .9;

var OUTLINE_COLOR          = "#FFFFFF";
var FILL_COLOR             = "#231f20";
var SELECT_OUTLINE_COLOR   = "#f64f37";
var SELECT_FILL_COLOR      = "#f64f37";
var POPUP_UNSELECTED_COLOR = "#f64f37";
var POPUP_SELECTED_COLOR   = "#f64f37";
var TEXT_COLOR             = "#231f20";
var HALO_COLOR             = "rgba(255,255,255,.3)";
var FLOOR_FONT = "Bold 12pt Arial";
var ARROW_FONT = "14pt Century Gothic, AppleGothic, sans-serif";

var DONGLE_LINE = 1;
var POPUP_LINE = 2;
var POPUP_RADIUS = 10;
var FLOOR_LINE = 1;

var KEY_SPEED = 15;
var DOUBLE_CLICK_THRESHOLD = 15;
var DC_X_THRESHOLD = 100;