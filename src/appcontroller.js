var AppController = function () {
    this.lastMouseDown = false;
    this.lastX = 0;
    this.lastY = 0;
    this.t = 0;

    //set floorButtons, panos, currentPano, and arrows  
    PanoContent.setup(this);
}

AppController.prototype = {
    update: function () {
        this.checkForButtonRollOver();

        var keyX=0;
        if (Key.isDown(37)) keyX += KEY_SPEED;
        if (Key.isDown(39)) keyX -= KEY_SPEED;
        this.currentPano.translate(keyX, 0);

        if(Mouse.leftDown && !this.lastMouseDown){
            //MOUSE DOWN
            this.lastX = Mouse.x;
            this.lastY = Mouse.y;
            this.processButtonDowns();

        }else if (Mouse.leftDown) {
            //MOUSE DRAG
            this.deltaX = Mouse.x - this.lastX;
            this.deltaY = Mouse.y - this.lastY;

            this.currentPano.translate(this.deltaX, this.deltaY);

            this.lastX = Mouse.x;
            this.lastY = Mouse.y;

        }else if(this.lastMouseDown) {
            //MOUSE RELEASE
            this.checkForButtonClicks();
        }

        if(this.isDoubleClicking()) {
            this.goToPanoNear(Mouse.x);
        }
        if(Key.isDown(38) && !this.lastUpKeyDown){
            this.goToPanoNear(WIDTH/2);
        }
        
        this.lastMouseDown = Mouse.leftDown;
        this.lastUpKeyDown = Key.isDown(38);

        this.t++;

        POPUP_RADIUS = Math.sin(this.t*.1)*5+10;
    },

    draw: function () {
        var self = this;

        // Draw the background
        context.fillStyle = "#FFFFFF";
        context.fillRect(0,0,WIDTH,HEIGHT);

        this.currentPano.draw(context);
        this.floorButtons.map(function(b){b.draw(context,self.currentPano);});

        
    },

    //prevents you from mouse pressing off button, then moving mouse onto button
    //and then releasing mouse button to activate button.
    processButtonDowns: function () {
        var self = this,
        arrowsAndButtons = this.currentPano.arrows.concat(this.floorButtons);

        arrowsAndButtons.map(function(button){
            button.underMouseDuringPress = button.containsMouse();
        });
    },

    checkForButtonClicks: function () {
        var self = this,
        arrowsAndButtons = this.currentPano.arrows.concat(this.floorButtons);

        arrowsAndButtons.map(function(button){
            if(button.containsMouse() && button.underMouseDuringPress){
                self.goToPano(button.destinationPano);
            }
        });
    },

    checkForButtonRollOver: function () {
        var self = this,
        arrowsAndButtons = this.currentPano.arrows.concat(this.floorButtons);

        arrowsAndButtons.map(function(button){
            button.rollOver = button.containsMouse();
        });

        this.currentPano.popups.map(function(popup){
            popup.expanded = popup.containsMouse(self.currentPano.x,self.currentPano.y);
            popup.update();
        });
    },

    isDoubleClicking: (function () {
        var lastClickTime = -100;
        var currentTime = 0;
        return function () {
            var did = false;
            currentTime ++;
            if(Mouse.leftDown && !this.lastMouseDown) {
                if(currentTime - lastClickTime < DOUBLE_CLICK_THRESHOLD){
                    did = true;
                }
                lastClickTime = (did ? -100 : currentTime); //if successful doubleclick, reset timer
            }
            return did;
        }
    })(),

    createPanoBridge: function (from, to, arrowLoc, msg){
        this.panos[from].addArrow(arrowLoc,this.panos[to],msg);
    },

    goToPano: function (pano) {
        var oldPano = this.currentPano;
        //reset rollovers for current pano so they don't blink when you return
        this.currentPano.arrows.map(function(a){a.rollOver = false;}); 
        this.currentPano = pano;
        this.currentPano.faceAwayFrom(oldPano);
    },

    // The math gets really confusing here, but basically it is seeing if the arrow.locOnFilm is within
    // DC_X_THRESHOLD of the mouse position.
    // It first finds relativeX which is the x position of the mouse with respect to the pano.
    // Then it finds the distance between the mouse and each of the arrow locations.
    // it has to do the (diff > pWidth/2) line to account for the wrap-around since the left and right
    // edges of the pano should be touching.
    goToPanoNear: function (x) {
        var self = this,
        pWidth = this.currentPano.width,
        relativeX = (x - this.currentPano.x+pWidth)%pWidth;

        this.currentPano.arrows.map(function(arrow){
             var diff = relativeX - arrow.locOnFilm;
             if (diff > pWidth/2){
                diff -= pWidth;
             }
             if(Math.abs(diff) < DC_X_THRESHOLD){
                self.goToPano(arrow.destinationPano);
             }
        });
       
    },

    addFloorButton:function(index) {
        var fb = new FloorButton(this.panos[index],this.floorButtons.length+1);
        fb.y =  10+this.floorButtons.length*30;
        this.floorButtons.push(fb);
    },

    addPopUp:function(pano_index,src,x,y,w,h,url){
        this.panos[pano_index].addPopUp(src,x,y,w,h,url);
    }
}