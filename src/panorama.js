var Panorama = function (src, w , h, name) {
    this.bitmap = Content.getImage(src);
    this.width = w;
    this.height = h;

    this.x = 0;
    this.y = 0;

    this.arrows = [];
    this.popups = [];

    this.areaName = name;

    this.floorNumber = this.getFloorFrom(src);
}

Panorama.prototype = {
    // add an arrow to the panorama
    // locOnFilm - where in pixel coordinates, the x position of the arrow is
    // destinationPano - clicking the arrow leads where?
    addArrow: function (locOnFilm, destinationPano, msg) {
        var arrow = new Arrow();
        arrow.locOnFilm = locOnFilm;
        arrow.destinationPano = destinationPano;
        if(!(typeof msg == "undefined")) {
            arrow.msg = "Go To "+ msg;
        }else if(!(typeof destinationPano.areaName == "undefined")){
            arrow.msg = "Go To "+ destinationPano.areaName;
        }else{
            arrow.msg = "";
        }
        this.arrows.push(arrow);
    },

    // move the pano
    translate : function (x,y) {
        this.x = (this.x + x + this.width)%this.width;
        this.y += y;
        if(this.y > 0) this.y = 0;
        if(this.y + this.height < HEIGHT) this.y =  HEIGHT - this.height;
    },

    // Given an x coordinate on this pano, which angle does that represent if
    // you wrap the rectangular image into a ring?
    getAngleGivenX: function (x) {
        x -= WIDTH/2;
        var ratio = (x+this.width)%this.width;
        ratio /= this.width;
        return -ratio * Math.PI*2;
    },

    // What angle is the panorama facing?
    getAngle: function () {
        return this.getAngleGivenX(this.x);
    },

    // Is the pano shifted up or down? returns result in interval [0,1]
    getVShift: function () {
        var ratio = (this.y+HEIGHT/2+this.height)%this.height;
        ratio /= this.height;
        return ratio;
    },

    // when you enter this panorama, naturally you want to be facing
    // the direction that you were walking to get here. To do this we face away from
    // the panorama we were at previously
    faceAwayFrom: function(pano) {
        var arrow;
        var x;

        for (var i = 0; i < this.arrows.length; i++) {
            arrow = this.arrows[i];
            if(arrow.destinationPano === pano){
                //x = position on film directly opposite the arrow
                x = (arrow.locOnFilm + this.width/2)%this.width;
                this.x = -x + WIDTH/2;
            }
        }
    },

    addPopUp: function (src,x,y,w,h, url){
        this.popups.push(new PopUp(src,x,y,w,h,this,url));
    },

    draw : function (ctx) {
        var self = this;
        ctx.drawImage(this.bitmap, this.x, this.y);
        ctx.drawImage(this.bitmap, this.x-this.width, this.y);
        ctx.drawImage(this.bitmap, this.x+this.width, this.y);
        this.drawArrows(ctx);

        //draw expanded popups last to ensure they are on top
        this.popups.map(function(popup){if(!popup.expanded){popup.draw(ctx,self.x,self.y)}});
        this.popups.map(function(popup){if(popup.expanded){popup.draw(ctx,self.x,self.y)}});
    },

    drawArrows: function (ctx) {
        var self = this;
        //for elipse:
        var x = WIDTH/2;
        var y = HEIGHT*DONGLE_LOCATION;
        var radius = 100;

        var cloudrad = radius+130;
        ctx.beginPath();
        ctx.fillStyle = HALO_COLOR;
        Utils.drawEllipseByCenter(ctx, x, y, cloudrad, cloudrad*this.arrows[0].getPerspective());
        ctx.fill();


        if(this.arrows.length!==0){ //prevent index out of bounds
            ctx.strokeStyle = OUTLINE_COLOR;
            ctx.lineWidth = DONGLE_LINE;

            ctx.beginPath();
            ctx.lineWidth = 8;
            ctx.strokeStyle = 'white';
            Utils.drawEllipseByCenter(ctx, x, y, radius, radius*this.arrows[0].getPerspective());
            ctx.closePath();
            ctx.stroke();

            ctx.beginPath();
            ctx.lineWidth = 6;
            ctx.strokeStyle = FILL_COLOR;
            Utils.drawEllipseByCenter(ctx, x, y, radius, radius*this.arrows[0].getPerspective());
            ctx.closePath();
            ctx.stroke();
        }

        this.arrows.map(function(arrow){
            arrow.angle = self.getAngleGivenX(self.x + arrow.locOnFilm);
            //arrow.vshift = self.getVShift();
            arrow.draw(ctx);
        });
    },

    //UGLY CODE so sorry :(
    getFloorFrom: function (src) {
        var i = src.length-6,

        //the last two characters of pano name e.g. 07
        panoNum = Number.parseInt(src.substring(i,i+2));

        if(panoNum < 12) return 3; //floor 3
        if(panoNum < 17) return 2; //floor 2
        return 1;
    }
}
