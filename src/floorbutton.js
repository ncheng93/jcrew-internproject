var FloorButton = function (dest,num) {
    this.destinationPano = dest;
    this.x = 0;
    this.y = 0;
    this.radius = 10;
    this.rollOver = false;
    this.blackImg = Content.getImage("images/floor_black_"+num+".png");
    this.orangeImg = Content.getImage("images/floor_orange_"+num+".png");

    this.draw = function (ctx, currentPano) {
        var isOnFloor = currentPano.floorNumber === num;

        if(!this.rollOver && !isOnFloor){
            ctx.drawImage(this.blackImg, 0, this.y);
        } else {
            ctx.drawImage(this.orangeImg, 0, this.y);
        }

    }

    this.containsMouse = function () {
        //circle button collision
        /*var xdis = Mouse.x - this.x;
        var ydis = Mouse.y - this.y;
        var dist = Math.sqrt(xdis*xdis+ydis*ydis);
        return dist < this.radius;*/

        //larger rectangular collision
        return Utils.pointInRect(this.x, //top left x of rect
                                 this.y, //top left y of rect
                                 125,       //width of rect
                                 30,        //height of rect
                                 Mouse.x,   //test point x
                                 Mouse.y);  //test point y
    }
}