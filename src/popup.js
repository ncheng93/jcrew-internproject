var PopUp = function(src,x,y,w,h, pano,url) {
    this.bitmap = Content.getImage(src);
    this.w = w;
    this.h = h;
    this.x = x;
    this.y = y;

    this.pano = pano;

    this.expanse = 0; //number between 0 and 1; 0=fully closed, 1=fully expanded
    this.expanded = false;

    this.expandSpeed = .05;
    this.shopBtn = new ShopButton(145,170,url);
}

PopUp.prototype = {
    containsMouse: function (xoff,yoff) {
        var w, h;

        if (this.expanded) {
            w = this.w;
            h = this.h;
        } else {
            w = 50;
            h = 50;
        }

        var xloc = (this.x+this.pano.x+this.pano.width)%this.pano.width -(w/2);

        return Utils.pointInRect(xloc, //top left x of rect
                                 this.y-(h/2)+yoff, //top left y of rect
                                 w,        //width of rect
                                 h,        //height of rect
                                 Mouse.x,   //test point x
                                 Mouse.y);  //test point y
    },

    update: function () {
        var xloc = (this.x+this.pano.x+this.pano.width)%this.pano.width,
            localX = Math.floor(xloc-this.w/2),
            localY = Math.floor(this.y-this.h/2+this.pano.y);

        if(this.expanded){
            if(this.expanse<1){
                this.expanse+=this.expandSpeed;
            }else{
                this.expanse = 1;
            }
        }else{
            if(this.expanse>0){
                this.expanse-=this.expandSpeed;
            }else{
                this.expanse = 0;
            }
        }

        this.shopBtn.update(localX,localY);
    },

    draw: function (ctx,px,py) {
        var xloc = (this.x+px+this.pano.width)%this.pano.width;

        //var i = this.expanse; //immediately expand
        var i = Math.max(0,this.expanse*2-1); //delay then expand
        
        ctx.lineWidth = POPUP_LINE;

        if(this.expanse ===0 || i ===0){
            ctx.beginPath();
            ctx.strokeStyle = (this.expanse === 0 ? POPUP_UNSELECTED_COLOR : POPUP_SELECTED_COLOR);
            ctx.arc(xloc,this.y+py, POPUP_RADIUS,0,Math.PI*2);
            ctx.closePath();
            ctx.stroke();
        }else if(this.expanse<1){
            ctx.strokeStyle = POPUP_UNSELECTED_COLOR;
            var w = this.w*i;
            var h = this.h*i;
            var x = xloc-w/2;
            var y = this.y+py-h/2;
            ctx.strokeRect(x,y,w,h);
        }else{
            var x = Math.floor(xloc-this.w/2);
            var y = Math.floor(this.y-this.h/2+py);
            ctx.drawImage(this.bitmap,x,y);
            this.shopBtn.draw(x,y);
        }
    }
}